# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: SILLOU Justin justin.sillou.etu@univ-lille.fr

## Question 1

Le processus ne peut pas écrire sur le fichier car il n'est qu'en lecture pour l'utilisateur toto (r--).  

## Question 2

- Le caractère x indiquant que le fichier est exécutable permet pour un répertoire de se rendre à l'intérieur. Sans cette autorisation, l'utilisateur n'aura pas la permission d'accèder au dossier.  
	
- L'utilisateur toto ne peut pas rentrer dans le fichier mydir car il appartient au groupe ubuntu et l'utilisateur ubuntu à retiré l'accès aux autre utilisateurs du groupe (à par lui même).  

	
- On peut lister les différents contenus dans le dossier mais on ne peut pas y accèder.  
N'ayant pas les droits d'executions, l'utilisateur toto ne peut pas avoir les informations sur les différents statistiques des fichiers.   

```
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt
```

## Question 3

``` 
cd question3
make
./suid <target>
```

- Les différents ids sont :  
```
euid : 1001
egid : 1000
ruid : 1001
rgid : 1000
```
Le processus n'arrive pas à ouvrir le fichier en lecture :   
`Cannot open file: Permission denied`

```
euid : 1000
egid : 1000
ruid : 1001
rgid : 1000
```
Le processus arrive à ouvrir le fichier en lecture :   
`File opens correctly`
	
## Question 4

```
cd question3
python3 suid.py
```

```
EUID :  1001
EGID :  1000
```

## Question 5

La commande **chfn** sert comme sont l'indique (**Ch**ange real user name and **in**formations)  
Changer les informations ainsi que le nom d'un utilisateur.  
  
`-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn`  

Le compte root à le droit en lecture écriture ainsi qu'un flag set-user-id ce qui lui permet le droit en execution. Ainsi qu'un doit en lecture et execution par le groupe root et les autres users.


## Question 6

En regardant dans `man passwd` il est indiqué que les info sécurisé sur les utilisateurs sont stockées dans `/etc/shadow`  
Ce fichier est disponible en lecture et écriture par l'user root.  
`-rw-r----- 1 root shadow 1156 Jan 12 15:32 /etc/shadow`  
Les mots de passe ne sont pas stockés en clair mais ce sont les hash des mots de passe qui y sont stockées.  

## Question 7
<!-- 
    Ajout commentaires dans les scripts pour expliquer ce qu'il font.
-->
Executer la question 7 en ligne de commande:
```
init.sh && directory.sh
```

**init.sh**  

  1. Création des utilisateurs : admin / lambda_a et lambda_b  
    `adduser admin` ...  

  2. Création des groupes : groupe_a , b et c  
    `addgroup groupe_a` ...  
   
  3. Ajout des utilisateurs aux groupes 
    `adduser admin groupe_a`

| groupe a    | groupe b | groupe c |
| ----------- | -------- | -------- |
| admin       | admin    | admin    |
| lambda_a    | lambda_b | lambda_a |
| *lambda_a2* |          | lambda_b |

Par la suite on pourra ajouter un utilisateur lambda_a2 dans le groupe_a afin de tester que les utilisateurs qui n'ont pas les droits ne peuvent pas renommer ni effacer les fichiers qui ne les appartiennenet pas.  

**directory.sh**  

On met amdin en titulaire des fichiers pour qu'il puisse tous les modifiers.
```
chown -R admin:groupe_a dir_a/
chown -R admin:groupe_b dir_b/
chown -R admin:groupe_c dir_c/
```

Pour que les fichiers ne puissent être modifiés que par le propiétaire et par les utilisateurs qui créent les fichiers, on va appriquer le sticky bit.
```
chmod +t <directory>
```

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








