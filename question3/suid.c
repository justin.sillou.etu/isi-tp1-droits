#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *f;
	printf("euid : %d\n", geteuid());
	printf("egid : %d\n", getegid());
	printf("ruid : %d\n", getuid());
	printf("rgid : %d\n", getgid());

	if (argc < 2) {
		printf("Missing argument\n");
		exit(EXIT_FAILURE);
	}

	f = fopen(argv[1], "r");
	if (f == NULL) {
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	printf("File opens correctly\n");
	fclose(f);
	exit(EXIT_SUCCESS);
	return 0;
}


