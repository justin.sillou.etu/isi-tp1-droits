# Création de l'administrateur et des groupes a et b
echo "creation de l'admin"
adduser admin 

# Creation des deux groupes
echo "creation des groupes a et b"
addgroup groupe_a
addgroup groupe_b

# Création d'un groupe groupe_c pour le dir_c 
echo "création du groupe c (pour le dir c)"
addgroup groupe_c

# Ajout de l'administrateur dans les deux groupes
echo "ajout de l'admin dans les groupes a, b et c"
adduser admin groupe_a && adduser admin groupe_b && adduser admin groupe_c

# Création des utilisateur lambda_a et lambda_b
echo "création des utilisateurs lambda_a et lambda_b et ajouts aux différents groupes"
adduser lambda_a --ingroup groupe_a
adduser lambda_b --ingroup groupe_b
adduser lambda_a groupe_c && adduser lambda_b groupe_c

