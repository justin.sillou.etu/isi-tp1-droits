#Donner les accès des répertoires à l'administrateur
echo "Droit d'accès des répertoires à l'administrateur"
chown -R admin:groupe_a dir_a/
chown -R admin:groupe_b dir_b/
chown -R admin:groupe_c dir_c/

#Droits des dossiers dir_a, dir_b et dir_c
chmod 770 dir_a/
chmod 770 dir_b/
chmod 771 dir_c/

#Mise en place du stickyBit
echo "mise en place du sticky bit"
chmod +t dir_a/
chmod +t dir_b/